## Resume

The aim of this program is to bruteforce the password of a Wordpress website with known admin username. Two methods can be use : alphanumeric or by dictionary. In both methods, a google browser will open and multiple password until it succed to log in.

## Technology
- Node Js 

## Libraries
- pupperteer
- fs

## Install and run project

Clone this project by running
```
git clone git@gitlab.com:MatthieuLV/bruteforce.git
```

Then run 
```
npm install
```

To use the alphanumeric method, run in the project folder
```
node ./bruteforceAlphanumeric.js
```

To use the dictionary method, run
```
node ./bruteforceDictionnary.js
```
